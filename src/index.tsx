
import { LogoutItem } from 'apprise-frontend/call/Logout';
import { icns } from "apprise-frontend/icons";
import { ExternalLink } from "apprise-frontend/scaffold/ExternalLink";
import { ButtonItem } from "apprise-frontend/scaffold/MenuItem";
import { Scaffold } from "apprise-frontend/scaffold/Scaffold";
import { Section } from "apprise-frontend/scaffold/Section";
import { Slider } from "apprise-frontend/scaffold/Slider";
import { tenants } from 'apprise-frontend/tenant/api';
import { tenantRoute } from "apprise-frontend/tenant/constants";
import { useUsers } from "apprise-frontend/user/api";
import { UserProfile, userprofileId, UserProfileItem } from "apprise-frontend/user/Profile";
import { messagePlural, messageType } from "apprise-messages/constants";
import { campaigns } from 'emaris-frontend/campaign/api';
import { acbanner, AcIcon, acRoute, iotcLogo, iotcRoute, McIcon, mcRoute, rcColor, RcIcon } from "emaris-frontend/constants";
import { dashboardIcon, dashboardName, dashboardRoute } from "emaris-frontend/dashboard/constants";
import { Dashboard } from "emaris-frontend/dashboard/Dashboard";
import { summaryName, summaryRoute } from "emaris-frontend/dashboard/ViewRouter";
import { eventRoute } from "emaris-frontend/event/constants";
import { products } from 'emaris-frontend/product/api';
import { productPlural, productRoute } from "emaris-frontend/product/constants";
import { requirements } from 'emaris-frontend/requirement/api';
import { requirementPlural, requirementRoute } from "emaris-frontend/requirement/constants";
import { HelpDrawer } from '#helpsheet';
import { homeIcon, homeRoute } from "#home/constants";
import React from 'react';
import { useTranslation } from "react-i18next";
import "./styles.scss";
import "./variables.css";

export const ReportingConsole = () => {

    const { t } = useTranslation()
    const { logged } = useUsers()

    const [helpDrawer, helpDrawerSet] = React.useState(false)

    return <Scaffold title={t("rc_home.title")} shortTitle={t("rc_home.short_title")} icon={<RcIcon color='white' /> } banner={acbanner} >

        <Section title={t("rc_home.name")} icon={homeIcon} path={homeRoute} showOnSider={false} exact
            crumbs={{

                [homeRoute]: { name: "" },
                [dashboardRoute]: {name : t(dashboardName)},
                [`${dashboardRoute}/*/${summaryRoute}`]: {name : t(summaryName)},

                [`${dashboardRoute}/*`]: {resolver:campaigns.breadcrumbResolver},
                [`${dashboardRoute}/*${requirementRoute}`]: { omit: true },
                [`${dashboardRoute}/*${requirementRoute}/*`]:{resolver:requirements.breadcrumbResolver},
                [`${dashboardRoute}/*/*/*${requirementRoute}`]: { name: t(requirementPlural) },
                [`${dashboardRoute}/*/*/*${requirementRoute}/*`]: {resolver:requirements.breadcrumbResolver},
                [`${dashboardRoute}/*${productRoute}`]: { omit: true },
                [`${dashboardRoute}/*${productRoute}/*`]:{resolver:products.breadcrumbResolver},
                [`${dashboardRoute}/*/*/*${productRoute}`]: { name: t(productPlural) },
                [`${dashboardRoute}/*/*/*${productRoute}/*`]: {resolver:products.breadcrumbResolver},
                [`${dashboardRoute}/*${tenantRoute}`]: { omit: true },
                [`${dashboardRoute}/*${tenantRoute}/*`]: { omit: true },
                [`${dashboardRoute}/*/*/*${tenantRoute}`]: { omit: true },
                [`${dashboardRoute}/*/*/*${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver },
                [`${dashboardRoute}/*${tenantRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${productRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${requirementRoute}/*/${summaryRoute}`]: {name : t(summaryName)},
                [`${dashboardRoute}/*${productRoute}/*/${messageType}`]: {name : t(messagePlural)},
                [`${dashboardRoute}/*${requirementRoute}/*/${messageType}`]: {name : t(messagePlural)},
                [`${dashboardRoute}/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                [`${dashboardRoute}/*/*/*${eventRoute}`]: { name: t("dashboard.calendar.name") },
                [`${dashboardRoute}/*/${messageType}`]: { name: t(messagePlural) },
                [`${dashboardRoute}/*/*/*/*/*/latest`]: { name: t("submission.labels.latest") },
                [`${dashboardRoute}/*/*/*/*/*/*`]: { name: t("submission.labels.old_version") }


            }}>
            <Dashboard />
        </Section>
        <Section title={t("dashboard.name")} icon={dashboardIcon} path={dashboardRoute}>
            <Dashboard />
        </Section>
        <Slider id={userprofileId}>
            <UserProfile />
        </Slider>
{/* 
        <Slider id={settingsId}>
            <Settings />
        </Slider> */}

        {UserProfileItem()}
        {/* {SettingsItem()} */}
        {LogoutItem()}

        <ButtonItem title={t("help.title")} icon={icns.help()} onClick={()=>helpDrawerSet(!helpDrawer)} style={{color: rcColor}}>
            <HelpDrawer open={helpDrawer} onClose={()=>helpDrawerSet(!helpDrawer)} />
        </ButtonItem>
        <ExternalLink title={t("links.iotc")} icon={iotcLogo} href={iotcRoute} />

        <ExternalLink enabled={logged.hasNoTenant()} title={t("links.mc")} icon={<McIcon />} href={mcRoute} />
        <ExternalLink enabled={logged.managesSomeTenant()} title={t("links.ac")} icon={<AcIcon />} href={acRoute} />

    </Scaffold>

}
