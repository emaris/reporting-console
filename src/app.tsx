import { EmarisState, initialEmariState } from 'emaris-frontend/state/model';

export type State = EmarisState;

export const state : State = { ...initialEmariState };