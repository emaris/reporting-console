
import { mockPermissions } from 'apprise-frontend/utils/mocks/iam';
import { mockSettings } from 'apprise-frontend/utils/mocks/settings';
import { mockTags } from 'apprise-frontend/utils/mocks/tags';
import { mockThemes } from 'apprise-frontend/utils/mocks/themes';
import { mockUsers } from 'apprise-frontend/utils/mocks/users';
import { mockMessages } from "apprise-messages/mocks";
import MockAdapter from "axios-mock-adapter";
import { mockCampaigns } from 'emaris-frontend/utils/mocks/campaigns';
import { mockEvents } from 'emaris-frontend/utils/mocks/events';
import { mockInstances } from 'emaris-frontend/utils/mocks/instances';
import { mockInfo } from 'emaris-frontend/utils/mocks/mocks';
import { mockProducts } from 'emaris-frontend/utils/mocks/products';
import { mockRequirements } from 'emaris-frontend/utils/mocks/requirements';
import { mockSubmissions } from 'emaris-frontend/utils/mocks/submissions';

export const mockery= (mock:MockAdapter) => {

  // mockConfig(mock,{
  //   routedTypes: [submissionType],

  //   mode: "dev",
  //   name:"rc",
  //   services: {
  //     admin:{"prefix":"admin",label:"Admin Backend"},
  //     domain:{"prefix":"domain",label:"Domain Backend",default:true}
  //   }, 
  //   intl:{ 
  //     languages:["en","fr"],
  //     required:["en","fr"]
  //   }

  // })

      
  mock.onAny().passThrough()

}


//eslint-disable-next-line
const mockEverything = (mock:MockAdapter) => {

  mockSettings(mock)
  mockTags(mock)
 
  mockEvents(mock)
  mockInfo(mock)
  mockPermissions(mock)
  mockUsers(mock)



  mockThemes(mock)
  mockRequirements(mock)
  
  mockProducts(mock)
  mockCampaigns(mock)

  mockInstances(mock)

  mockSubmissions(mock)

  mockMessages(mock)

  mock.onAny().passThrough()
}

