import Title from "antd/lib/typography/Title";
import { Page } from "apprise-frontend/scaffold/Page";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import * as React from "react";
import { useTranslation } from "react-i18next";
import "./styles.scss";




export const Home =  () => {

  const {t} = useTranslation()


   return <Page className="apppage" headerClassName="appheader">

   <Sidebar/>

    <Title level={2} className="applogo"> {"[e-MARIS logo]"}</Title>

    <div className="appbanner">
    </div>

    <Title className="apptitle">{t("rc_home.title")}</Title> 
     
      </Page>
  
}