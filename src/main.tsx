
import { ReportingConsole } from '#index';
import { state } from "#app";
import { App } from "apprise-frontend/App";
import { layoutmodule } from "apprise-frontend/layout/module";
import { PushEvents } from 'apprise-frontend/push/PushEvents';
import { tagmodule } from "apprise-frontend/tag/module";
import { tenantmodule } from "apprise-frontend/tenant/module";
import { usermodule } from "apprise-frontend/user/module";
import { messagemodule } from "apprise-messages/module";
import { CampaignPreloader } from 'emaris-frontend/campaign/Loader';
import { campaignmodule, compliancemodule, timelinessmodule } from "emaris-frontend/campaign/module";
import { submissionModule } from "emaris-frontend/campaign/submission/module";
import { eventmodule } from "emaris-frontend/event/module";
import { productmodule } from "emaris-frontend/product/module";
import { requirementmodule } from "emaris-frontend/requirement/module";
import { mockery } from '#mockery';
import ReactDOM from 'react-dom';
import "./styles.scss";
import "./variables.css";

const modules = [usermodule, tagmodule, eventmodule, tenantmodule, messagemodule, requirementmodule, productmodule, campaignmodule, timelinessmodule, layoutmodule, submissionModule, compliancemodule]



ReactDOM.render(

    <App initState={state} modules={modules} mockery={mockery}>
        <PushEvents>
            <CampaignPreloader />
            <ReportingConsole />
        </PushEvents>
    </App>

    , document.getElementById('root'));