import { Icon } from 'antd'
import { Button } from 'apprise-frontend/components/Button'
import { Drawer } from 'apprise-frontend/components/Drawer'
import { Paragraph } from 'apprise-frontend/components/Typography'
import { icns } from 'apprise-frontend/icons'
import { messageIcon, messageType } from 'apprise-messages/constants'
import { useCurrentCampaign } from 'emaris-frontend/campaign/hooks'
import { SubmissionReactContext } from 'emaris-frontend/campaign/submission/hooks'
import { useCurrentAsset, useCurrentParty, useDashboard } from 'emaris-frontend/dashboard/hooks'
import { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import * as goicns from 'react-icons/go'

export const HelpDrawer = (props: { open: boolean, onClose: () => void }) => {

    const campaign = useCurrentCampaign()

    return campaign ? <InCampaignHelpDrawer {...props} /> : <Inner {...props} />
}


const InCampaignHelpDrawer = (props: { open: boolean, onClose: () => void }) => {

    const dashboard = useDashboard()

    const { party } = useCurrentParty()
    const { asset } = useCurrentAsset()

    const submission = useContext(SubmissionReactContext)?.submission

    const messageRoute = party ?

        asset && submission?.trail ?
            `${dashboard.partyRouteToSubmissionWith(asset.instanceType, asset.source, party.source)}?tab=${messageType}`
            : `${dashboard.routeToParty(party)}/${messageType}`

        : undefined

    return <Inner messageRoute={messageRoute} {...props} />
}


const Inner = (props: { open: boolean, messageRoute?: string, onClose: () => void }) => {

    const { open, onClose, messageRoute } = props

    const { t } = useTranslation()

    return <Drawer icon={icns.help()} title={t("help.title")} width={600} visible={open} closable={true} onClose={onClose}>
        <div className="help-view">
            <Paragraph className="explainer">{t('help.explainer')}</Paragraph>
            <div className='help-actions'>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.guides.title")}</div>
                    <div className='blurb-explainer'>{t("help.guides.explainer")}</div>
                    <Button enabledOnReadOnly icn={<Icon component={goicns.GoLinkExternal} />} iconLeft type='primary' target="_blank" className='blurb-action' href={t("help.guides.action")}>{t("help.guides.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.general.title")}</div>
                    <div className='blurb-explainer'>{t("help.general.explainer")}</div>
                    <Button enabledOnReadOnly icn={icns.mail} iconLeft type='primary' className='blurb-action' href={t("help.general.action")}>{t("help.general.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.technical.title")}</div>
                    <div className='blurb-explainer'>{t("help.technical.explainer")}</div>
                    <Button enabledOnReadOnly icn={icns.mail} iconLeft type='primary' className='blurb-action' href={t("help.technical.action")}>{t("help.technical.action_title")}</Button>
                </div>
                <div className='help-blurb'>
                    <div className='blurb-title'>{t("help.campaign.title")}</div>
                    <div className='blurb-explainer'>{t("help.campaign.explainer")}</div>
                    <Button enabledOnReadOnly icn={messageIcon} iconLeft type='primary' className='blurb-action' linkTo={messageRoute} disabled={!messageRoute}>{t("help.campaign.action_title")}</Button>
                </div>
            </div>
        </div>
    </Drawer>
}

